-- DROP DATABASE IF EXISTS `studentenhuis`;
-- CREATE DATABASE `studentenhuis`;
-- USE "2152912";

--
-- Uncomment de volgende SQL statements om een user in de database te maken
-- Vanwege security mag je die user alleen in je lokale ontwikkeldatabase aanmaken!
-- Op een remote 'productie'-server moet je zorgen voor een ANDER useraccount!
-- Vanuit je (bv. nodejs) applicatie stel je de credentials daarvan in via environment variabelen.
--
-- studentenhuis_user aanmaken
--CREATE USER 'studenten_user'@'%' IDENTIFIED BY 'secret';
--CREATE USER 'studenten_user'@'localhost' IDENTIFIED BY 'secret';

-- geef rechten aan deze user
--GRANT SELECT, INSERT, DELETE, UPDATE ON `21529122152912studentenhuis`.* TO 'studenten_user'@'%';
--GRANT SELECT, INSERT, DELETE, UPDATE ON `studentenhuis`.* TO 'studenten_user'@'localhost';

-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user` ;
CREATE TABLE IF NOT EXISTS `user` (
	`ID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`Voornaam` VARCHAR(32) NOT NULL,
	`Achternaam` VARCHAR(32) NOT NULL,
	`Email` VARCHAR(32) NOT NULL,
	`Studentnummer` VARCHAR(32) NOT NULL,
	`Password` CHAR(64) BINARY NOT NULL,
	`ImageUrl` VARCHAR(256),
	`ImagePath` VARCHAR(256),
	PRIMARY KEY (`ID`)
) 
ENGINE = InnoDB;

-- Voorbeeld insert query. Wanneer je in Nodejs de ? variant gebruikt hoeven de '' niet om de waarden.
-- Zet die dan wel in het array er na, in de goede volgorde.
-- In je Nodejs app zou het password wel encrypted moeten worden.
INSERT INTO `user` (Voornaam, Achternaam, Email, Studentnummer, Password) VALUES ('Jan', 'Smit', 'jsmit@server.nl', '2222222', '$2b$10$Pg4p1bs25b64UmjSk9nk.OpouIztkaFtHT3/PoZlNDRy.83nZyTFy');
INSERT INTO `user` (Voornaam, Achternaam, Email, Studentnummer, Password) VALUES ('Bas', 'Voss', 'voss.bas@gmail.com', '2152912', '$2b$10$JX4OMxsjD53IPIEkalI.Y.wSd21wXSWt1I25ZUjNhhkWRcwFKho1q');

-- -----------------------------------------------------
-- Table `studentenhuis`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `studentenhuis` ;
CREATE TABLE IF NOT EXISTS `studentenhuis` (
	`ID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`Naam` VARCHAR(32) NOT NULL,
	`Adres` VARCHAR(32),
	`UserID` INT UNSIGNED NOT NULL,
	`Zipcode` VARCHAR(256),
	`Phone` VARCHAR(256),
	`Place` VARCHAR(256),
	`ImageUrl` VARCHAR(256),
	`ImagePath` VARCHAR(256),
	PRIMARY KEY (`ID`)
) 
ENGINE = InnoDB;

ALTER TABLE `studentenhuis` 
ADD CONSTRAINT `fk_studentenhuis_user`
FOREIGN KEY (`UserID`) REFERENCES `user` (`ID`)
ON DELETE NO ACTION
ON UPDATE CASCADE;

-- Voorbeeld insert query. Wanneer je in Nodejs de ? variant gebruikt hoeven de '' niet om de waarden.
INSERT INTO `studentenhuis` (`Naam`, `Adres`, `UserID`, `Zipcode`, `Phone`,`Place`) VALUES 
('Princenhage', 'Princenhage, Breda', 1,'4706RQ','061234567891','Roosendaal'),
('Haagdijk 23', 'Haagdijk, Breda', 2, '4706KN','061234567891','Roosendaal'),
('Den Hout', 'Lovensdijkstraat, Den Hout', 1, '4706RZ','061234567891','Roosendaal'),
('Den Dijk', 'Langendijk, Breda', 1, '4706PQ','061234567891','Roosendaal'),
('Lovensdijk', 'Lovensdijkstraat, Breda', 1, '4706UR','061234567891','Roosendaal'),
('Van Schravensteijn', 'Schravensteijnseweg 23, Breda', 1, '4706AE','061234567891','Roosendaal');

-- -----------------------------------------------------
-- Table `maaltijd`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `maaltijd` ;
CREATE TABLE IF NOT EXISTS `maaltijd` (
	`ID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`Naam` VARCHAR(32) NOT NULL,
	`Beschrijving` VARCHAR(64) NOT NULL,
	`Ingredienten` VARCHAR(64) NOT NULL,
	`Allergie` VARCHAR(32) NOT NULL,
	`AangemaaktOp` DATE NOT NULL,
	`AangebodenOp` DATE NOT NULL,
	`Prijs` DOUBLE UNSIGNED  NOT NULL,
	`UserID` INT UNSIGNED NOT NULL,
	`StudentenhuisID` INT UNSIGNED NOT NULL,
	`ImageUrl` VARCHAR(256),
	`ImagePath` VARCHAR(256),
	PRIMARY KEY (`ID`)
) 
ENGINE = InnoDB;

ALTER TABLE `maaltijd` 
ADD CONSTRAINT `fk_maaltijd_user`
FOREIGN KEY (`UserID`) REFERENCES `user` (`ID`)
ON DELETE NO ACTION
ON UPDATE CASCADE
,
ADD CONSTRAINT `fk_maaltijd_studentenhuis`
FOREIGN KEY (`StudentenhuisID`) REFERENCES `studentenhuis` (`ID`)
ON DELETE NO ACTION
ON UPDATE CASCADE
;

-- Voorbeeld insert query.
INSERT INTO `maaltijd` (Naam, Beschrijving, Ingredienten, Allergie,AangemaaktOp,AangebodenOp, Prijs, UserID, StudentenhuisID) VALUES 
('Zuurkool met worst', 'Zuurkool a la Montizaan, specialiteit van het huis.', 'Zuurkool, worst, spekjes', 'Lactose, gluten','2020-09-01','2020-09-01', 5, 1, 1),
('Spaghetti', 'Spaghetti Bolognese', 'Pasta, tomatensaus, gehakt', 'Lactose','2020-09-01','2020-09-01', 3, 1, 1);
-- Voorbeeld delete query
-- DELETE FROM `maaltijd` WHERE `Naam` = 'Spaghetti';

-- -----------------------------------------------------
-- Table `deelnemers`
-- Bevat de users die deelnemen aan een maaltijd in een studentenhuis.
-- 
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deelnemers` ;
CREATE TABLE IF NOT EXISTS `deelnemers` (
	`UserID` INT UNSIGNED NOT NULL,
	`StudentenhuisID` INT UNSIGNED NOT NULL,
	`MaaltijdID` INT UNSIGNED NOT NULL,
	`AangemeldOp` DATE NOT NULL,
	PRIMARY KEY (`UserID`, `StudentenhuisID`, `MaaltijdID`)
) 
ENGINE = InnoDB;

ALTER TABLE `deelnemers` 
ADD CONSTRAINT `fk_deelnemers_user`
FOREIGN KEY (`UserID`) REFERENCES `user` (`ID`)
ON DELETE NO ACTION
ON UPDATE CASCADE
,
ADD CONSTRAINT `fk_deelnemers_studentenhuis`
FOREIGN KEY (`StudentenhuisID`) REFERENCES `studentenhuis` (`ID`)
ON DELETE NO ACTION
ON UPDATE CASCADE
,
ADD CONSTRAINT `fk_deelnemers_maaltijd`
FOREIGN KEY (`MaaltijdID`) REFERENCES `maaltijd` (`ID`)
ON DELETE NO ACTION
ON UPDATE CASCADE
;

-- Voorbeeld insert query.
-- Let op: je kunt je maar 1 keer aanmelden voor een maaltijd in een huis.
-- Je kunt je natuurlijk wel afmelden en opnieuw aanmelden. .
-- INSERT INTO `deelnemers` (UserID, StudentenhuisID, MaaltijdID) VALUES (1, 1, 1);
-- Voorbeeld van afmelden:
-- DELETE FROM `deelnemers` WHERE UserID = 1 AND StudentenhuisID = 1 AND MaaltijdID = 1;
-- En opnieuw aanmelden:
-- INSERT INTO `deelnemers` (UserID, StudentenhuisID, MaaltijdID) VALUES (1, 1, 1);

-- -----------------------------------------------------
-- View om deelnemers bij een maaltijd in een studentenhuis in te zien.
-- 
-- -----------------------------------------------------
CREATE OR REPLACE VIEW `view_studentenhuis` AS 
SELECT 
	`studentenhuis`.`ID`,
	`studentenhuis`.`Naam`,
	`studentenhuis`.`Adres`,
	`studentenhuis`.`Zipcode`,
	`studentenhuis`.`Phone`,
	`studentenhuis`.`Place`,
	`studentenhuis`.`ImageUrl`,
	CONCAT(`user`.`Voornaam`, ' ', `user`.`Achternaam`) AS `Contact`,
	`user`.`Email`,
	`user`.`Studentnummer`
FROM `studentenhuis`
LEFT JOIN `user` ON `studentenhuis`.`UserID` = `user`.`ID`;

SELECT * FROM `view_studentenhuis`;

-- -----------------------------------------------------
-- View om deelnemers bij een maaltijd in een studentenhuis in te zien.
-- 
-- -----------------------------------------------------
CREATE OR REPLACE VIEW `view_deelnemers` AS 
SELECT 
	`deelnemers`.`StudentenhuisID`,
	`deelnemers`.`MaaltijdID`,
	`user`.`Voornaam`,
	`user`.`Achternaam`,
	`user`.`Email`,
	`user`.`Studentnummer`,
	`user`.`ImageUrl`
FROM `deelnemers`
LEFT JOIN `user` ON `deelnemers`.`UserID` = `user`.`ID`;

-- Voorbeeldquery.
SELECT * from `view_deelnemers` WHERE StudentenhuisID = 1 AND MaaltijdID = 1; 

ALTER TABLE `user` ADD UNIQUE( `Email`);
ALTER TABLE `studentenhuis` ADD UNIQUE `unique_index`( `Adres`,`Zipcode`);