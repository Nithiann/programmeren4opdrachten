// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');
const tracer = require('tracer');
const pool = require('../../src/config/pool');
const request = require('supertest');

const testAccount = {
    email: 'jsmit@server.nl',
    password: 'MySecretpassword1',
};

const authenticatedUser = request.agent(server);

const CLEAR_DB = 'DELETE IGNORE FROM maaltijd';
const INSERT_QUERY = `INSERT INTO maaltijd (ID, Naam, Beschrijving, Ingredienten, Allergie,AangemaaktOp,AangebodenOp, Prijs, UserID, StudentenhuisID) VALUES 
(1, 'Zuurkool met worst', 'Zuurkool a la Montizaan, specialiteit van het huis.', 'Zuurkool, worst, spekjes', 'Lactose, gluten','2020-09-01','2020-09-01', 5, 2, 1),
(2, 'Spaghetti', 'Spaghetti Bolognese', 'Pasta, tomatensaus, gehakt', 'Lactose','2020-09-01','2020-09-01', 3, 1, 1);`;

// Configure chai
chai.use(chaiHttp);
chai.should();
tracer.setLevel('trace');

let token;

before((done) => {
    request(server)
        .post('/api/login')
        .send(testAccount)
        .end((err, resp) => {
            token = `Bearer  ${resp.body.authorisation}`;
            done();
        });
});

describe('manage student home meals', () => {
    beforeEach((done) => {
        // console.log('beforeEach')
        pool.query(CLEAR_DB, (err, rows, fields) => {
            if (err) {
                console.log(`beforeEach CLEAR error: ${err}`);
                done(err);
            } else {
                pool.query(INSERT_QUERY, (err, rows, fields) => {
                    if (err) {
                        console.log(`beforeEACH INSERT error: ${err}`);
                        done(err);
                    } else {
                        done();
                    }
                });
            }
        });
    });

    after((done) => {
        pool.query(CLEAR_DB, (err, rows, fields) => {
            if (err) {
                console.log(`after error: ${err}`);
                done(err);
            } else {
                pool.query(INSERT_QUERY, (err, rows, fields) => {
                    if (err) {
                        console.log(`beforeEACH INSERT error: ${err}`);
                        done(err);
                    } else {
                        done();
                    }
                });
            }
        });
    });

    describe('UC-301 - Add a meal', () => {
        it('TC-301-1 - mandatory fields are missing', (done) => {
            authenticatedUser
                .set('token', token)
                .post('/api/studenthome/1/meal')
                .send({
                    name: 'stampot',
                    offer: '28-04-2020',
                    price: '3,50',
                    allergies: 'geen aparte componenten',
                    ingredients: 'aardappels, penen, saus',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing!');

                    done();
                });
        });

        it('TC-301-2 - should return error if data is missing', (done) => {
            authenticatedUser
                .set('token', token)
                .post('/api/studenthome/1/meal')
                .send({
                    name: 'stampot',
                    ingredients: 'aardappels, penen, saus',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing!');

                    done();
                });
        });

        it('TC-301-3 - should return error if array is empty', (done) => {
            authenticatedUser
                .set('token', token)
                .post('/api/studenthome/1/meal')
                .send({
                    name: 'stampot',
                    offer: '28-04-2020',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing!');

                    done();
                });
        });

        it('TC-301-4 - should return error if meal already exists', (done) => {
            authenticatedUser
                .set('token', token)
                .post('/api/studenthome/1/meal')
                .send({
                    name: 'Zuurkool met worst',
                    description: 'Zuurkool a la Montizaan, specialiteit van het huis.',
                    made_on: '2020-09-01',
                    offer: '2020-09-01  ',
                    price: 3.5,
                    allergies: 'Lactose, gluten',
                    ingredients: 'Zuurkool, worst, spekjes',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('meal already exists.');

                    done();
                });
        });

        it('TC-301-5 - user is not logged in', (done) => {
            // there is no authentication
            authenticatedUser
                .set('token', 'token is weird')
                .post('/api/studenthome/1/meal')
                .send({
                    name: 'Bami',
                    description: 'Noodles met groente en stukjes ei',
                    made_on: '28-04-2020',
                    offer: '28-04-2020',
                    price: 4.35,
                    allergies: 'ei',
                    ingredients: 'ei, noodles, groente',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(401);

                    let { message } = res.body;
                    message.should.be.an('String');

                    done();
                });
        });

        it('TC-301-6 - should return results, added to db', (done) => {
            authenticatedUser
                .set('token', token)
                .post('/api/studenthome/1/meal')
                .send({
                    name: 'Bami',
                    description: 'Noodles met groente en stukjes ei',
                    made_on: '28-04-2020',
                    offer: '28-04-2020',
                    price: 4.35,
                    allergies: 'ei',
                    ingredients: 'ei, noodles, groente',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(200);

                    let { data } = res.body;
                    data.should.be.an('object');

                    done();
                });
        });
    });

    describe('UC-302 - change a meal', () => {
        it('TC-302-1 - should return error when data is missing.', (done) => {
            authenticatedUser
                .set('token', token)
                .put('/api/studenthome/1/meal/1')
                .send({
                    name: 'Roti',
                    allergies: 'geen aparte componenten',
                    ingredients: 'aardappels, vlees, ',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message, error } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing!');
                    error.should.be.an('string');

                    done();
                });
        });

        it('TC-302-2 - should return error when not valid data', (done) => {
            authenticatedUser
                .set('token', token)
                .put('/api/studenthome/1/meal/1')
                .send({
                    name: 'Roti',
                    allergies: 'geen aparte componenten',
                    ingredients: ['aardappels', 'saus', 'vlees'],
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message, error } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing!');
                    error.should.be.an('string');

                    done();
                });
        });

        it('TC-302-3 - should return error when date is invalid', (done) => {
            authenticatedUser
                .set('token', token)
                .put('/api/studenthome/1/meal/1')
                .send({
                    name: 'Bami',
                    description: 'Noodles met groente en stukjes ei',
                    made_on: '35-02-2020',
                    offer: '90-10-2020',
                    ingredients: 'ei, noodles, groente',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message, error } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing!');
                    error.should.be.an('string');

                    done();
                });
        });

        it('TC-302-4 - should return error when meal doesnt exist', (done) => {
            authenticatedUser
                .set('token', token)
                .put('/api/studenthome/1/meal/420')
                .send({
                    name: 'Roti',
                    description: 'vlees en saus en aardappels',
                    made_on: '28-04-2020',
                    offer: '28-04-2020',
                    price: 3.5,
                    allergies: 'geen aparte componenten',
                    ingredients: 'aardappels, vlees, knoflook',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('Meal does not exist.');

                    done();
                });
        });

        it('TC-302-5 - should return error if user is not logged in', (done) => {
            // no authentication yet
            authenticatedUser
                .set('token', 'Bearer t2hgfwug3748nv374e8hgfnt')
                .put('/api/studenthome/1/meal/420')
                .send({
                    name: 'Roti',
                    description: 'vlees en saus en aardappels',
                    made_on: '28-04-2020',
                    offer: '28-04-2020',
                    price: 3.5,
                    allergies: 'geen aparte componenten',
                    ingredients: 'aardappels, vlees, knoflook',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(401);

                    let { error } = res.body;
                    error.should.be.an('string').that.equals('Not allowed');

                    done();
                });
        });

        it('TC-302-6 - should return data if pushed', (done) => {
            authenticatedUser
                .set('token', token)
                .put('/api/studenthome/1/meal/2')
                .send({
                    name: 'Roti',
                    description: 'vlees en saus en aardappels',
                    made_on: '20-06-2020',
                    offer: '20-06-2020',
                    price: 3.5,
                    allergies: 'geen aparte componenten',
                    ingredients: 'aardappels, vlees, saus',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(200);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('meal has been updated');

                    done();
                });
        });
    });

    describe('UC-303 - list of all meals', () => {
        it('TC-303-1 - should return all meals', (done) => {
            authenticatedUser
                .set('token', token)
                .get('/api/studenthome/1/meal')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(200);

                    let { data } = res.body;
                    data.should.be.an('array');

                    done();
                });
        });
    });

    describe('UC-304 - details of a meal', () => {
        it('TC-304-1 - should return valid error when meal doesnt exist', (done) => {
            authenticatedUser
                .set('token', token)
                .get('/api/studenthome/1/meal/420')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('meal was not found!');

                    done();
                });
        });

        it('TC-304-2 - should return data when meal exists', (done) => {
            authenticatedUser
                .set('token', token)
                .get('/api/studenthome/1/meal/2')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(200);

                    let { meal } = res.body;
                    meal.should.be.an('array');

                    done();
                });
        });
    });

    describe('UC-305 - deleting a meal by id', () => {
        it('TC-305-1 - should return error when meal doesnt exist', (done) => {
            authenticatedUser
                .set('token', token)
                .delete('/api/studenthome/1/meal/420')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(404);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('meal does not exist.');

                    done();
                });
        });

        it('TC-305-2 - show error when user is not logged in', (done) => {
            authenticatedUser
                .set('token', 'Bearer sfbshfkusnf38s9ev98svnuvn984s')
                .delete('/api/studenthome/1/meal/1')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(401);

                    let { error } = res.body;
                    error.should.be.an('string').that.equals('Not allowed');

                    done();
                });
        });

        it('TC-305-3 - actor is not owner', (done) => {
            authenticatedUser
                .set('token', token)
                .delete('/api/studenthome/1/meal/1')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(401);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('User not authorized');

                    done();
                });
        });

        it('TC-305-4 - should return when meal is deleted', (done) => {
            authenticatedUser
                .set('token', token)
                .delete('/api/studenthome/1/meal/2')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(200);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('successfully deleted');

                    done();
                });
        });
    });
});
