const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');
const tracer = require('tracer');
const logger = require('../../src/config/config').logger;
const pool = require('../../src/config/pool');

//TC-101 - Register
describe('Authorisation', () => {
    after((done) => {
        pool.query('DELETE FROM user WHERE email = "jansen.2jan@gmail.com"', (err, result) => {
            if (err) logger.debug(err.toString());
            done();
        });
    });
    describe('TC-101 - Register - POST /api/register', () => {
        it('TC-101-1 - not all mandatory data send', (done) => {
            chai.request(server)
                .post('/api/register')
                .send({
                    firstName: 'Henk',
                    lastName: 'Bernards',
                    email: 'bernards.henk@gmail.com',
                    studentNumber: '2222222',
                    password: '',
                })
                .end((err, res) => {
                    res.should.be.an('object');

                    const { message } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing.');

                    done();
                });
        });

        it('TC-101-2 - email has incorrect format', (done) => {
            chai.request(server)
                .post('/api/register')
                .send({
                    firstName: 'Henk',
                    lastName: 'Bernards',
                    email: 'bernards.henk@gmail',
                    studentNumber: '1234678',
                    password: 'HelloWorld2020',
                })
                .end((err, res) => {
                    res.should.be.an('object');

                    const { message, error } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing.');
                    error.should.be
                        .an('string')
                        .that.equals('AssertionError [ERR_ASSERTION]: Email is not valid.');

                    done();
                });
        });

        it('TC-101-3 - studentnumber has incorrect format', (done) => {
            chai.request(server)
                .post('/api/register')
                .send({
                    firstName: 'Henk',
                    lastName: 'Bernards',
                    email: 'bernards.henk@gmail.com',
                    studentNumber: '12345678',
                    password: 'HelloWorld2020',
                })
                .end((err, res) => {
                    res.should.be.an('object');

                    const { message, error } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing.');
                    error.should.be
                        .an('string')
                        .that.equals(
                            'AssertionError [ERR_ASSERTION]: Studentnummer uses max 7 numbers',
                        );

                    done();
                });
        });

        it('TC-101-4 - password has incorrect format', (done) => {
            chai.request(server)
                .post('/api/register')
                .send({
                    firstName: 'Henk',
                    lastName: 'Bernards',
                    email: 'bernards.henk@gmail.com',
                    studentNumber: '1234678',
                    password: 'hllooooo',
                })
                .end((err, res) => {
                    res.should.be.an('object');

                    const { message, error } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing.');
                    error.should.be
                        .an('string')
                        .that.equals(
                            'AssertionError [ERR_ASSERTION]: Password needs to be atleast 6 characters long, atleast 1 number and uppercase letter',
                        );

                    done();
                });
        });

        it('TC-101-5 - gebruiker bestaat al', (done) => {
            chai.request(server)
                .post('/api/register')
                .send({
                    firstName: 'jan',
                    lastName: 'Voss',
                    email: 'jsmit@server.nl',
                    studentNumber: '2222222',
                    password: 'Im2x5xpmdd',
                })
                .end((err, res) => {
                    res.should.be.an('object');

                    const { message } = res.body;
                    message.should.be.an('string').that.equals('User already exists');

                    done();
                });
        });

        it('TC-101-6 - User successfully added', (done) => {
            chai.request(server)
                .post('/api/register')
                .send({
                    firstName: 'Jan',
                    lastName: 'Jansen',
                    email: 'jansen.2jan@gmail.com',
                    studentNumber: '1236722',
                    password: 'HelloWorld2020',
                })
                .end((err, res) => {
                    res.should.be.an('object');

                    const { authorisation } = res.body;
                    authorisation.should.be.an('string');

                    done();
                });
        });
    });

    describe('UC-102 Login - POST /api/login', () => {
        it('TC-102-1 - mandatory data is missing', (done) => {
            chai.request(server)
                .post('/api/login')
                .send({
                    email: 'jan.2jansen@gmail.com',
                })
                .end((err, res) => {
                    res.should.be.an('object');

                    const { message, error } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing.');
                    error.should.be.an('string');

                    done();
                });
        });

        it('TC-102-2 - invalid email', (done) => {
            chai.request(server)
                .post('/api/login')
                .send({
                    email: 'freekvonk@hotmail',
                    password: 'HelloWorld2020',
                })
                .end((err, res) => {
                    res.should.be.an('object');

                    const { message, error } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing.');
                    error.should.be
                        .an('string')
                        .that.equals('AssertionError [ERR_ASSERTION]: Email is not valid.');

                    done();
                });
        });

        it('TC-102-3 - invalid password', (done) => {
            chai.request(server)
                .post('/api/login')
                .send({
                    email: 'jansen.2jan@gmail.com',
                    password: 'HelloWorld2019',
                })
                .end((err, res) => {
                    res.should.be.an('object');

                    const { message } = res.body;
                    message.should.be.an('string').that.equals('Password is incorrect');

                    done();
                });
        });

        it('TC-102-4 - User doesnt exist', (done) => {
            chai.request(server)
                .post('/api/login')
                .send({
                    email: 'freekvonk@hotmail.com',
                    password: 'HelloWorld2020',
                })
                .end((err, res) => {
                    res.should.be.an('object');

                    const { message } = res.body;
                    message.should.be.an('string').that.equals('No user with this email.');

                    done();
                });
        });

        it('TC-101-5 - login success', (done) => {
            chai.request(server)
                .post('/api/login')
                .send({
                    email: 'jsmit@server.nl',
                    password: 'MySecretpassword1',
                })
                .end((err, res) => {
                    res.should.be.an('object');

                    const { authorisation } = res.body;
                    authorisation.should.be.an('string');

                    done();
                });
        });
    });
});
