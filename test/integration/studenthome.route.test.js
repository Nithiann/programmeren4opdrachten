// Import the dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');
const tracer = require('tracer');
const logger = require('../../src/config/config').logger;
const pool = require('../../src/config/pool');
const request = require('supertest');

const testAccount = {
    email: 'jsmit@server.nl',
    password: 'MySecretpassword1',
};

const authenticatedUser = request.agent(server);

const CLEAR_DB = 'DELETE IGNORE FROM studentenhuis';
const INSERT_QUERY = `INSERT INTO studentenhuis (ID, naam, adres, UserId, Zipcode, Phone, Place) VALUES 
(2, 'Huijbergseweg 63', 'Huijbergseweg, Hoogerheide', 2, '4631GD','0629766333','Hoogerheide'),
(3, 'KLM straat 21', 'KLMstraat, hoogerheide', 1, '3322AA','061234567891','Hoogerheide'),
(4, 'Jan jansen', 'bergsedijk, Breda', 1, '4706AA','061234567891','Roosendaal'),
(5, 'kip', 'Lovensdijkstraat, Breda', 1, '4706UR','06123123123','Roosendaal'),
(6, 'Jansen', 'schravenhaagsestraat 21, Breda', 1, '4706AG','061234567891','Roosendaal');`;

// Configure chai
chai.use(chaiHttp);
chai.should();
tracer.setLevel('trace');

let token;

before((done) => {
    request(server)
        .post('/api/login')
        .send(testAccount)
        .end((err, resp) => {
            token = `Bearer  ${resp.body.authorisation}`;
            done();
        });
});

describe('Manage studentHomes', () => {
    beforeEach((done) => {
        pool.query(CLEAR_DB, (err, rows, fields) => {
            if (err) {
                console.log(`beforeEach CLEAR error: ${err}`);
                done();
            } else {
                pool.query(INSERT_QUERY, (err, rows, fields) => {
                    done();
                });
            }
        });
    });

    afterEach((done) => {
        pool.query(CLEAR_DB, (err, rows, fields) => {
            if (err) {
                console.log(`after error: ${err}`);
                done(err);
            } else {
                pool.query(INSERT_QUERY, (err, rows, fields) => {
                    done();
                });
            }
        });
    });

    describe('UC-201 - Create student home - POST /api/studenthome', () => {
        it('TC-201-1 - should return valid error when mandatory fields are missing', (done) => {
            authenticatedUser
                .set('token', token)
                .post('/api/studenthome')
                .send({
                    name: 'frank',
                    street: 'allstreet',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing!');

                    done();
                });
        });

        it('TC-201-2 - should return valid error when mandatory zipcode is missing', (done) => {
            authenticatedUser
                .set('token', token)
                .post('/api/studenthome')
                .send({
                    name: 'Princenhage',
                    street: 'Princenhage, Breda',
                    zipcode: '4706RQA',
                    city: 'Roosendaal',
                    phone: '0629755444',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing!');

                    done();
                });
        });

        it('TC-201-3 - should return valid error when mandatory phone number is missing', (done) => {
            authenticatedUser
                .set('token', token)
                .post('/api/studenthome')
                .send({
                    name: 'Princenhage',
                    street: 'Princenhage, Breda',
                    zipcode: '4706RQ',
                    city: 'Roosendaal',
                    phone: '068604',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing!');

                    done();
                });
        });

        it('TC-201-4 - should return valid error when studenthome already exists', (done) => {
            authenticatedUser
                .set('token', token)
                .post('/api/studenthome')
                .send({
                    name: 'Princenhage',
                    street: 'Princenhage, Breda',
                    zipcode: '4706RQ',
                    city: 'Roosendaal',
                    phone: '0612345678',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('studenthome already exists.');

                    done();
                });
        });

        it('TC-201-5 - should get error when not signed in', (done) => {
            // no registration or login function in application
            authenticatedUser
                .set('token', 'no-token')
                .post('/api/studenthome')
                .send({
                    name: 'Princenhage',
                    street: 'Princenhage, Breda',
                    zipcode: '4706RQ',
                    city: 'Roosendaal',
                    phone: '0686047569',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(401);

                    let { error } = res.body;
                    error.should.be.an('string').that.equals('Not allowed');

                    done();
                });
        });

        it('TC-201-6 - should return correct result when added', (done) => {
            authenticatedUser
                .set('token', token)
                .post('/api/studenthome')
                .send({
                    name: 'Den Helder',
                    street: 'Amsterdamse Straat 26',
                    zipcode: '3312DD',
                    city: 'Den Haag',
                    phone: '0624523385',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(200);

                    let { message, data } = res.body;
                    message.should.be.an('string').that.equals('Studenthome has been added!');
                    data.should.be.an('object');

                    done();
                });
        });
    });

    describe('UC-202 - overview of studenthomes - GET /api/studenthome', () => {
        it('TC-202-1 - should return 0 studenthomes with OK code', (done) => {
            authenticatedUser
                .set('token', token)
                .get('/api/studenthome')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(200);

                    let { data } = res.body;
                    data.should.be.an('array');

                    done();
                });
        });

        it('TC-202-2 - should return multiple student homes with OK code', (done) => {
            authenticatedUser
                .set('token', token)
                .get('/api/studenthome?city=Roosendaal')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(200);

                    let { data } = res.body;

                    data.should.be.an('array');

                    done();
                });
        });

        it('TC-202-3 - should return error because of unknown city', (done) => {
            authenticatedUser
                .set('token', token)
                .get('/api/studenthome?city=Rotterdam')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(404);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('No student homes found');

                    done();
                });
        });

        it('TC-202-4 - should return error because of unknown name', (done) => {
            authenticatedUser
                .set('token', token)
                .get('/api/studenthome?name=hetKippenhok')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(404);

                    logger.debug(res.body.message);
                    let { message } = res.body;
                    message.should.be.an('string').that.equals('No student homes found');

                    done();
                });
        });

        it('TC-202-5 - should return result because of known city', (done) => {
            authenticatedUser
                .set('token', token)
                .get('/api/studenthome?city=Roosendaal')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(200);

                    let { data } = res.body;
                    data.should.be.an('array');

                    done();
                });
        });

        it('TC-202-6 - should return result because of known name', (done) => {
            authenticatedUser
                .set('token', token)
                .get('/api/studenthome?name=Princenhage')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(200);

                    logger.debug(res.body.message);
                    let { data } = res.body;
                    data.should.be.an('array');

                    done();
                });
        });
    });

    describe('UC-203 - details of studenthome - GET /api/studenthome/:homeId', () => {
        it('TC-203-1 - should return error message when no home found', (done) => {
            authenticatedUser
                .set('token', token)
                .get('/api/studenthome/1231')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(404);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('No student homes found');

                    done();
                });
        });

        it('TC-203-2 - should return result message when home found', (done) => {
            authenticatedUser
                .set('token', token)
                .get('/api/studenthome/1')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(200);

                    let { results } = res.body;
                    results.should.be.an('array');

                    done();
                });
        });
    });

    describe('UC-204 - Update student home - PUT /api/studenthome/:homeID', () => {
        it('TC-204-1 - should return valid error when mandatory fields are missing', (done) => {
            authenticatedUser
                .set('token', token)
                .put('/api/studenthome/1')
                .send({
                    name: 'frank',
                    street: 'allstreet',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing!');

                    done();
                });
        });

        it('TC-204-2 - should return valid error when mandatory zipcode is missing', (done) => {
            authenticatedUser
                .set('token', token)
                .put('/api/studenthome/2')
                .send({
                    name: 'Princenhage',
                    street: 'Princenhage, Breda',
                    zipcode: '4706RQA',
                    city: 'Roosendaal',
                    phone: '0629755444',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing!');

                    done();
                });
        });

        it('TC-204-3 - should return valid error when mandatory phone number is missing', (done) => {
            authenticatedUser
                .set('token', token)
                .put('/api/studenthome/2')
                .send({
                    name: 'Princenhage',
                    street: 'Princenhage, Breda',
                    zipcode: '4706RQ',
                    city: 'Roosendaal',
                    phone: '062975544444',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('Error! data is missing!');

                    done();
                });
        });

        it('TC-204-4 - should return valid error when studenthome doesnt exists', (done) => {
            authenticatedUser
                .set('token', token)
                .put('/api/studenthome/420')
                .send({
                    name: 'de witte bus1',
                    street: 'boeimeersingel',
                    zipcode: '4811AA',
                    city: 'Breda',
                    phone: '0629766333',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(400);

                    let { message } = res.body;
                    message.should.be
                        .an('string')
                        .that.equals('No studenthome exists with that id.');

                    done();
                });
        });

        it('TC-204-5 - should get error when not signed in', (done) => {
            // no registration or login function in application
            authenticatedUser
                .set('token', 'token')
                .put('/api/studenthome/2')
                .send({
                    name: 'Princenhage',
                    street: 'Princenhage, Breda',
                    zipcode: '4706RQ',
                    city: 'Roosendaal',
                    phone: '0686047569',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(401);

                    let { error } = res.body;
                    error.should.be.an('string').that.equals('Not allowed');

                    done();
                });
        });

        it('TC-204-6 - should return correct result when added', (done) => {
            authenticatedUser
                .set('token', token)
                .put('/api/studenthome/3')
                .send({
                    name: 'de zwarte vleugels',
                    street: 'benjaminstraat 15',
                    zipcode: '4631gg',
                    city: 'Breda',
                    phone: '0629766333',
                })
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(200);

                    let { message } = res.body;
                    message.should.be.an('string');

                    done();
                });
        });
    });

    describe('UC-205 - Delete student home - DELETE /api/studenthome/:homeId', () => {
        it('TC-205-1 - should return error if studenthome doesnt exist', (done) => {
            authenticatedUser
                .set('token', token)
                .delete('/api/studenthome/420')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(404);

                    let { message } = res.body;
                    message.should.be
                        .an('string')
                        .that.equals('No studenthome exists with that id.');

                    done();
                });
        });

        it('TC-205-2 - no user logged in', (done) => {
            // no authentication yet
            authenticatedUser
                .set('token', 'rdtfyguhjinkesrdxtvfgbn')
                .delete('/api/studenthome/3')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(401);

                    let { error } = res.body;
                    error.should.be.an('string').that.equals('Not allowed');

                    done();
                });
        });

        it('TC-205-3 - Actor is niet de eigenaar', (done) => {
            // no authentication yet
            authenticatedUser
                .set('token', token)
                .delete('/api/studenthome/2')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(401);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('User not authorized.');

                    done();
                });
        });

        it('TC-205-4 - studenthome has been deleted successfully', (done) => {
            authenticatedUser
                .set('token', token)
                .delete('/api/studenthome/4')
                .end((err, res) => {
                    res.should.be.an('object');
                    res.should.have.status(200);

                    let { message } = res.body;
                    message.should.be.an('string').that.equals('Studenthome has been deleted.');

                    done();
                });
        });
    });
});
