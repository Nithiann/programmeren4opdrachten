const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = process.env.PORT || 3000;
const logConfig = require('./src/config/config');
const logger = logConfig.logger;

// init body-parsing
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Router imports
const routes = require('./src/routes/routes');

// project listens to port
app.listen(port, () => {
    logger.info('server is listening on port ' + port);
});

// activate route middlewares
app.use('/api', routes);
app.get('*', (req, res) => {
    res.status(404).send('API endpoint not found.');
});

module.exports = app;
