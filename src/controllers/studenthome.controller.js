const logConfig = require('../config/config');
const logger = logConfig.logger;
const db = require('../config/database');
const assert = require('assert');

exports.validateStudentHome = (req, res, next) => {
    logger.info('validating body');
    try {
        const { name, street, zipcode, city, phone } = req.body;
        assert(typeof name === 'string', 'Name is missing');
        assert(typeof street === 'string', 'Street is missing!');
        assert(typeof zipcode === 'string', 'Zipcode us missing');
        assert.match(zipcode, /^(?:NL-)?(\d{4})\s*([A-Z]{2})$/i, 'zipcode not correct format');
        assert(typeof city === 'string', 'City is missing!');
        assert(typeof phone === 'string', 'Phone is missing!');
        assert.match(phone, /^([0-9]{10})$/, 'your phonenumber is too long or short');
        next();
    } catch (err) {
        res.status(400).json({
            message: 'Error! data is missing!',
            error: err.toString(),
        });
    }
};

// UC-201 Create studenthome
exports.newStudentHome = (req, res) => {
    logger.info('Creating new student home');

    let studentHome = req.body;

    logger.info('Pushing new student home');

    let check = `SELECT * FROM studentenhuis WHERE naam = ?`;

    try {
        db.query(check, [studentHome.name], function (err, result) {
            if (err) logger.debug(err.toString());
            else {
                logger.debug(result);
                if (result.length > 0) {
                    res.status(400).json({
                        message: 'studenthome already exists.',
                    });
                } else {
                    let sql = `INSERT INTO studentenhuis (Naam, Adres, UserID, Zipcode, Phone, Place) VALUES (?, ?, ?, ?, ?, ?)`;
                    logger.debug(sql);
                    db.query(
                        sql,
                        [
                            studentHome.name,
                            studentHome.street,
                            req.userId,
                            studentHome.zipcode,
                            studentHome.phone,
                            studentHome.city,
                        ],
                        function (err, result) {
                            if (err)
                                res.status(400).json({
                                    message: err.toString(),
                                });
                            res.status(200).json({
                                message: 'Studenthome has been added!',
                                data: result,
                            });
                        },
                    );
                }
            }
        });
    } catch (err) {
        res.status(500).json({
            message: err.toString(),
        });
    }
};

// UC-202 show studenthomes
exports.getStudentHomes = (req, res) => {
    // check if query parameters are given
    logger.info('Looping through db');
    logger.debug('name of person: ' + req.query.name);

    // loop through db to find correct items
    var sql;
    if (req.query.name) {
        sql = `SELECT * FROM studentenhuis WHERE naam = "${req.query.name}" AND UserID = ${req.userId}`;
    }

    if (req.query.city) {
        sql = `SELECT * FROM studentenhuis WHERE Place = "${req.query.city}" AND UserID = ${req.userId}`;
    }

    if (!req.query.city && !req.query.name) {
        sql = `SELECT * FROM studentenhuis WHERE UserID = ${req.userId}`;
    }

    logger.info('checking DB');
    db.query(sql, function (err, result) {
        if (err) logger.debug(err);
        else {
            logger.debug(result);
            if (result.length > 0) {
                res.status(200).json({
                    message: 'found homes',
                    data: result,
                });
            } else {
                res.status(404).json({
                    message: 'No student homes found',
                    data: [],
                });
            }
        }
    });

    logger.info('Sending response');
};

// UC-203 details of studenthome
exports.getHomeById = (req, res) => {
    logger.info('getting homeID parameter');
    let homeId = req.params.homeId;
    console.log('homeid ' + homeId);

    logger.info('checking if homeId exists and response');
    // find object and send back
    let sql = `SELECT * FROM studentenhuis WHERE ID = ?`;
    db.query(sql, [homeId], (err, result) => {
        if (err) logger.debug(err);
        else {
            logger.debug(result);
            if (result.length > 0) {
                res.status(200).json({
                    message: 'found home(s)',
                    results: result,
                });
            } else {
                res.status(404).json({
                    message: 'No student homes found',
                });
            }
        }
    });
};

// UC-204 update studenthome
exports.updateStudentHome = (req, res) => {
    logger.info('getting homeID parameter');

    let homeId = req.params.homeId;

    logger.info('creating basic model');
    // basic layout of studenthome
    let studentHome = req.body;

    logger.debug('new student home: ' + studentHome);

    logger.info('find row in db');

    let sql = `SELECT * FROM studentenhuis WHERE id = ?`;
    db.query(sql, [homeId], (err, checkResult) => {
        if (err) logger.err(err);
        else {
            if (checkResult.length == 0) {
                res.status(400).json({
                    message: 'No studenthome exists with that id.',
                    status: 400,
                });
            } else {
                if (checkResult[0].UserID === req.userId) {
                    logger.info('creating update SQL');
                    let updateSQL = `UPDATE studentenhuis SET Naam = ?, Adres = ?, UserID = ?, Zipcode = ?, Phone = ?, Place = ? WHERE ID = ?`;
                    db.query(
                        updateSQL,
                        [
                            studentHome.name,
                            studentHome.street,
                            req.userId,
                            studentHome.zipcode,
                            studentHome.phone,
                            studentHome.city,
                            homeId,
                        ],
                        (err, result) => {
                            if (err) logger.error(err);
                            else {
                                logger.debug(result);
                                if (result.changedRows === 1) {
                                    res.status(200).json({
                                        message: 'Home has been changed.',
                                    });
                                } else {
                                    res.status(400).json({
                                        message: 'the address is already in the database',
                                    });
                                }
                            }
                        },
                    );
                } else {
                    res.status(401).json({
                        message: 'User not authorized',
                    });
                }
            }
        }
    });
};

// UC-205 delete studenthome
exports.deleteStudentHome = (req, res) => {
    logger.info('checking if param exists');
    // get home id from request
    let homeId = req.params.homeId;

    logger.info('finding index of homeId ' + homeId);

    let sql = `SELECT * FROM studentenhuis WHERE id = ?`;
    db.query(sql, [homeId], (err, result) => {
        logger.info('checking if studenthome exists');
        if (result.length === 0) {
            res.status(404).json({
                message: 'No studenthome exists with that id.',
                status: 404,
            });
        } else {
            logger.debug('UserId', result[0]);
            logger.debug('req userId', req.userId);
            if (result[0].UserID === req.userId) {
                var deleteSQL = `DELETE FROM studentenhuis WHERE ID = ?`;
                logger.info('starting delete query');
                db.query(deleteSQL, [homeId], (err, result) => {
                    if (err) logger.debug(err.toString());
                    else {
                        res.status(200).json({
                            message: 'Studenthome has been deleted.',
                        });
                    }
                });
            } else {
                res.status(401).json({
                    message: 'User not authorized.',
                });
            }
        }
    });
};
