const logConfig = require('../config/config');
const logger = logConfig.logger;
const db = require('../config/database');
const jwt = require('jsonwebtoken');
const assert = require('assert');
const bcrypt = require('bcrypt');
const salt = 10;

exports.validateRegister = (req, res, next) => {
    try {
        logger.info('validating req data');
        const { firstName, lastName, email, studentNumber, password } = req.body;

        assert(typeof firstName === 'string', 'Firstname is missing!');
        assert(typeof lastName === 'string', 'Lastname is missing!');
        assert(typeof email === 'string', 'Email is missing!');
        assert.match(
            email,
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            'Email is not valid.',
        );
        assert(typeof studentNumber === 'string', 'Studentnumber is missing!');
        assert.match(studentNumber, /^([0-9]){7}$/, 'Studentnummer uses max 7 numbers');
        assert(typeof password === 'string', 'Password is missing!');
        assert.match(
            password,
            /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/,
            'Password needs to be atleast 6 characters long, atleast 1 number and uppercase letter',
        );
        next();
    } catch (err) {
        res.status(400).json({
            message: 'Error! data is missing.',
            error: err.toString(),
        });
    }
};

exports.validateLogin = (req, res, next) => {
    try {
        logger.info('validating req data');
        const { email, password } = req.body;
        assert(typeof email === 'string', 'Email is missing');
        assert.match(
            email,
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            'Email is not valid.',
        );
        assert(typeof password === 'string', 'Password is missing');
        next();
    } catch (err) {
        res.status(400).json({
            message: 'Error! data is missing.',
            error: err.toString(),
        });
    }
};

// UC-101 - Registreren
exports.register = (req, res) => {
    logger.info('creating a new account');
    let user = req.body;
    logger.debug('registring user: ' + user.firstName);
    logger.info('checking if user exists');
    let checkSQL = 'SELECT * FROM user WHERE Email = ? OR Studentnummer = ?';
    db.query(checkSQL, [user.email, user.studentNumber], (err, result) => {
        logger.debug('does it have a user?', result > 0 ? 'yes' : 'no');
        if (result.length > 0) {
            res.status(400).json({
                message: 'User already exists',
            });
        } else {
            if (err) res.status(500).json({ error: err.toString() });
            logger.info('creating insert SQL');
            let insertSQL = `INSERT INTO user (Voornaam, Achternaam, Email, StudentNummer, Password) VALUES (?, ?, ?, ?, ?)`;
            logger.info('generating salt using bcrypt');
            bcrypt.genSalt(salt, (err, salt) => {
                if (err) res.status(400).json({ error: err.toString });
                logger.info('generating password with salt');
                bcrypt.hash(user.password, salt, (err, hashedPass) => {
                    if (err) res.status(400).json({ error: err.toString });
                    logger.info('activating insertSQL with hashed pass');
                    db.query(
                        insertSQL,
                        [user.firstName, user.lastName, user.email, user.studentNumber, hashedPass],
                        (err, result) => {
                            if (err) logger.debug(err.toString());
                            try {
                                logger.info('creating payload');
                                const payload = {
                                    userId: result.insertId,
                                };

                                logger.info('creating json token');
                                const auth = {
                                    authorisation: jwt.sign(payload, 'my_Secret_Password', {
                                        expiresIn: '2h',
                                    }),
                                    username: user.firstName + ' ' + user.lastName,
                                };

                                res.status(200).json(auth);
                            } catch (err) {
                                res.status(400).json({
                                    error: err.toString,
                                    message: 'Failed to add user!',
                                });
                            }
                        },
                    );
                });
            });
        }
    });
};

//UC-102 - Inloggen
exports.login = (req, res) => {
    logger.info('creating login attempt object');
    let user = req.body;
    let checkSQL = 'SELECT * FROM user WHERE Email = ?';
    logger.info('checking if user exists by email');
    db.query(checkSQL, [user.email], (err, result) => {
        if (err) res.status(400).json({ error: err.toString });

        if (result.length === 1) {
            logger.info('checking password');
            bcrypt.compare(user.password, result[0].Password, (err, correct) => {
                if (err) res.status(400).json({ error: err.toString });

                if (correct) {
                    logger.info('password is correct, preparing json token');
                    logger.debug(result[0]);
                    const payload = {
                        id: result[0].ID,
                    };

                    logger.info('creating json token');
                    const auth = {
                        authorisation: jwt.sign(payload, 'my_Secret_Password', { expiresIn: '2h' }),
                        username: result[0].Voornaam + ' ' + result[0].Achternaam,
                    };

                    res.status(200).json(auth);
                } else {
                    res.status(400).json({
                        message: 'Password is incorrect',
                    });
                }
            });
        } else {
            logger.info('user doesnt exist');
            res.status(400).json({
                message: 'No user with this email.',
            });
        }
    });
};

exports.validateToken = (req, res, next) => {
    logger.info('start checking token');
    var token;
    let authHeader;
    if (req.headers.token.length > 8) {
        authHeader = req.headers.token;
        token = authHeader.substring(8, authHeader.length);
    } else {
        res.status(401).json({
            error: 'Not allowed',
            message: 'Please try logging in first',
        });
    }

    logger.debug(authHeader);
    logger.info('removing Bearer from token');

    logger.debug('token', token);

    jwt.verify(token, 'my_Secret_Password', (err, payload) => {
        if (err) {
            logger.warn('Not authorized');
            res.status(401).json({
                error: 'Not allowed',
                message: 'Please try relogging',
            });
        }

        if (payload) {
            logger.debug('token is valid', payload);
            req.userId = payload.id;
            next();
        }
    });
};
