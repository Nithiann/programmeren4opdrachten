const logger = require('../config/config').logger;

exports.login = (req, res) => {
    logger.info('start login route');
    res.send('login authentication');
};

exports.register = (req, res) => {
    logger.info('start register route');
    res.send('register authentication');
};

exports.info = (req, res) => {
    logger.info('start info route');
    res.send({
        naam: 'Bas Voss',
        studentNummer: '2152912',
        beschrijving: 'web server in het kader van Avans Informatica jaar 1 Periode 4',
        'SonarQube URL':
            'https://sonarqube.avans-informatica-breda.nl/dashboard?id=nodejs-samen-eten_BV',
    });
};
