const logConfig = require('../config/config');
const logger = logConfig.logger;
const db = require('../config/database');
const assert = require('assert');
const moment = require('moment');

exports.validateMeal = (req, res, next) => {
    try {
        const { name, description, made_on, offer, price, allergies, ingredients } = req.body;
        assert(typeof name === 'string', 'Name is missing!');
        assert(typeof description === 'string', 'Description is missing');
        assert(typeof made_on === 'string', 'Date is missing!');
        assert(typeof offer === 'string', 'Offering date is missing!');
        assert(typeof price === 'number', 'Price is missing!');
        assert(typeof allergies === 'string', 'Allergies are missing!');
        assert(typeof ingredients === 'string', 'Ingredients are mnissing');
        next();
    } catch (err) {
        res.status(400).json({
            message: 'Error! data is missing!',
            error: err.toString(),
        });
    }
};

//UC-301 Maaltijd aanmaken
exports.addMeal = (req, res) => {
    logger.info('adding new meal');

    logger.debug(`Date: ${moment(req.body.made_on, 'DD/MM/YYYY').format('DD-MM-YYYY')}`);

    try {
        let meal = {
            name: req.body.name,
            description: req.body.description,
            made_on: moment(req.body.made_on, 'DD/MM/YYYY').format('DD-MM-YYYY'),
            offer: moment(req.body.offer, 'DD/MM/YYYY').format('DD-MM-YYYY'),
            price: req.body.price,
            allergies: req.body.allergies,
            ingredients: req.body.ingredients,
        };
        logger.debug('created meal: ' + meal.name);

        let check = `SELECT * FROM maaltijd WHERE naam = ?`;
        db.query(check, [meal.name], (err, result) => {
            if (err) logger.error(err);
            else {
                if (result.length > 0) {
                    res.status(400).json({
                        message: 'meal already exists.',
                    });
                    return;
                } else {
                    let url = req.originalUrl;
                    let path = url.split('/');
                    logger.info('checking if user has access');
                    let checkSQL = 'SELECT * FROM studentenhuis WHERE id = ?';
                    logger.debug('id', path[3]);
                    db.query(checkSQL, [path[3]], (err, check) => {
                        if (err) res.status(400).json(err);
                        if (check.length > 0) {
                            if (check[0].UserID === req.userId) {
                                logger.info('creating SQL string');
                                let sql = `INSERT INTO maaltijd (Naam, Beschrijving, Ingredienten, Allergie, AangemaaktOp, AangebodenOp, Prijs, UserId, StudentenhuisId) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`;
                                db.query(
                                    sql,
                                    [
                                        meal.name,
                                        meal.description,
                                        meal.ingredients,
                                        meal.allergies,
                                        meal.made_on,
                                        meal.offer,
                                        meal.price,
                                        req.userId,
                                        path[3],
                                    ],
                                    (err, result) => {
                                        if (err) logger.error(err);
                                        else {
                                            res.status(200).json({
                                                message: 'data successfully inserted',
                                                data: result,
                                            });
                                            return;
                                        }
                                    },
                                );
                            } else {
                                res.status(401).json({
                                    message: 'User not authorized',
                                });
                            }
                        }
                    });
                }
            }
        });
    } catch (err) {
        res.status(500).json({
            message: err.toString(),
            status: 500,
        });
    }
};

//UC-303 Lijst van maaltijden
exports.getMeals = (req, res) => {
    logger.info('meals being send back');

    let path = req.originalUrl;
    let paths = path.split('/');
    logger.debug(`Studenthome: ${paths[3]}`);

    let sql = `SELECT * FROM maaltijd WHERE StudentenhuisID = ?`;
    db.query(sql, [paths[3]], (err, result) => {
        if (err) logger.error(err);
        else {
            if (result.length > 0) {
                res.status(200).json({
                    data: result,
                });
            } else {
                res.status(400).json({
                    message: 'meal was not found!',
                });
            }
        }
    });
};

//UC-302 Update maaltijd
exports.updateMealById = (req, res) => {
    let meal = req.body;

    let sql = `SELECT * FROM maaltijd WHERE ID = ?`;
    db.query(sql, [req.params.mealId], (err, result) => {
        if (err) logger.error(err);
        else {
            logger.debug(`found meal: ${result.naam}`);
            if (result.length === 0) {
                res.status(400).json({
                    message: 'Meal does not exist.',
                });
            } else {
                if (result[0].UserID === req.userId) {
                    logger.info('creating update query');
                    let path = req.originalUrl;
                    let paths = path.split('/');
                    logger.debug('studenthome id', paths[3]);
                    let updateSQL = `UPDATE maaltijd SET Naam = ?, Beschrijving = ?, Ingredienten = ?, Allergie = ?, AangemaaktOp = ?, AangebodenOp = ?, Prijs = ?, UserID = ?, StudentenhuisId = ? WHERE ID = ?`;
                    db.query(
                        updateSQL,
                        [
                            meal.name,
                            meal.description,
                            meal.ingredients,
                            meal.allergies,
                            meal.made_on,
                            meal.offer,
                            meal.price,
                            req.userId,
                            paths[3],
                            req.params.mealId,
                        ],
                        (err, result) => {
                            if (err) logger.error(err);
                            else {
                                logger.info('changed');
                                if (result.changedRows === 1) {
                                    res.status(200).json({
                                        message: 'meal has been updated',
                                    });
                                } else {
                                    res.status(400).json({
                                        message: 'the meal already exists for this house',
                                    });
                                }
                            }
                        },
                    );
                } else {
                    res.status(401).json({
                        message: 'User not authorized',
                    });
                }
            }
        }
    });
};

//UC-304 MaaLtijd ophalen
exports.getMealById = (req, res) => {
    logger.info('getting meal by id');
    logger.debug('request meal by id => ' + req.params.mealId);

    let sql = `SELECT * FROM maaltijd WHERE ID = ?`;
    db.query(sql, [req.params.mealId], (err, result) => {
        if (err) logger.error(err);
        else {
            if (result.length > 0) {
                res.status(200).json({
                    meal: result,
                });
            } else {
                res.status(400).json({
                    message: 'meal was not found!',
                });
            }
        }
    });
};

//UC-305 Verwijder maaltijd
exports.deleteMealById = (req, res) => {
    var checkSQL = `SELECT * FROM maaltijd WHERE ID = ?`;
    db.query(checkSQL, [req.params.mealId], (err, result) => {
        if (err) logger.error(err);
        else {
            if (result.length === 0) {
                res.status(404).json({
                    message: 'meal does not exist.',
                });
            } else {
                logger.info('checking user access');
                if (result[0].UserID === req.userId) {
                    logger.debug('db user', result[0].UserID);

                    logger.info('user has access');
                    let deleteSQL = `DELETE FROM maaltijd WHERE ID = ?`;
                    db.query(deleteSQL, [req.params.mealId], (err, result) => {
                        if (err) logger.debug(err.toString());
                        res.status(200).json({
                            message: 'successfully deleted',
                        });
                    });
                } else {
                    res.status(401).json({
                        message: 'User not authorized',
                    });
                }
            }
        }
    });
};
