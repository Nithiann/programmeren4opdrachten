var mysql = require('mysql');
var dbConfig = require('../config/config').dbconfig;
var logger = require('../config/config').logger;

var connection = mysql.createConnection(dbConfig);

connection.connect(function (err) {
    if (err) logger.debug(err.toString()); // not connected!
    console.log('connected as id ' + connection.threadId);
});

module.exports = connection;
