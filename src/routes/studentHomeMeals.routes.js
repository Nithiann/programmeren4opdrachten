const express = require('express');
const router = express.Router();
const controller = require('../controllers/studenthomeMeal.controller');
const authController = require('../controllers/authentication.controller');

router.post(
    '/',
    authController.validateToken,
    controller.validateMeal,
    controller.addMeal,
);
router.get('/', authController.validateToken, controller.getMeals);

router.put(
    '/:mealId',
    (req, res, next) => {
        if (!req.params.mealId) {
            res.status(400).json({
                message: 'no mealId found',
                status: 400,
            });
        }
        next();
    },
    authController.validateToken,
    controller.validateMeal,
    controller.updateMealById,
);

router.get(
    '/:mealId',
    (req, res, next) => {
        if (!req.params.mealId) {
            res.status(400).json({
                message: 'no mealId found',
                status: 400,
            });
        }
        next();
    },
    controller.getMealById,
);

router.delete(
    '/:mealId',
    (req, res, next) => {
        if (!req.params.mealId) {
            res.status(400).json({
                message: 'no mealId found',
                status: 400,
            });
        }
        next();
    },
    authController.validateToken,
    controller.deleteMealById,
);

module.exports = router;
