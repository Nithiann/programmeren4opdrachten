const express = require('express');
const router = express.Router();
const controller = require('../controllers/studenthome.controller');
const authController = require('../controllers/authentication.controller');

router.get('/', authController.validateToken, controller.getStudentHomes);

router.post(
    '/',
    authController.validateToken,
    controller.validateStudentHome,
    controller.newStudentHome,
);

router.get('/:homeId', authController.validateToken, controller.getHomeById);

router.put(
    '/:homeId',
    authController.validateToken,
    controller.validateStudentHome,
    controller.updateStudentHome,
);

router.delete(
    '/:homeId',
    authController.validateToken,
    controller.deleteStudentHome,
);

module.exports = router;
