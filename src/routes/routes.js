const express = require('express');
const router = express.Router();
const studentHome = require('./studenthome.routes');
const apiController = require('../controllers/api.controller');
const studentHomeMeals = require('./studentHomeMeals.routes');
const authController = require('../controllers/authentication.controller');

// different routes
router.use('/studenthome', studentHome);
router.use('/studenthome/:homeId/meal', studentHomeMeals);

// main routes
router.post('/login', authController.validateLogin, authController.login);
router.post('/register', authController.validateRegister, authController.register);
router.get('/info', apiController.info);

module.exports = router;
